FROM registry.gitlab.com/spreadspace/docker/foundation/debian:bookworm

RUN set -x \
    && apt-get update -q \
    && apt-get install -y -q nginx-light libnginx-mod-stream libnginx-mod-rtmp libnginx-mod-http-lua gawk curl \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

COPY scripts/ /usr/local/bin/

USER app
