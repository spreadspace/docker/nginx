#!/bin/bash

INTERVAL=${INTERVAL:-10}

if [ -z "$1" ]; then
   echo "Usage: $0 <base-url>"
   exit 1
fi
BASE_URL="$1"

while sleep $INTERVAL; do
  variant_playlists=$(curl -sf "$BASE_URL/index.m3u8"  | gawk 'match($0,/\.m3u8$/) { print($0) }; match($0, /URI="(.*\.m3u8)"/, m) { print m[1] }')
  cnt=0
  failed=0
  for playlist in $variant_playlists; do
    last_segment=$(curl -sf "$BASE_URL/$playlist" | gawk 'match($0,/\.ts$/) { print($0) }' | tail -1)
    curl -sf "$BASE_URL/$(dirname $playlist)/$last_segment" > /dev/null
    if [ $? -ne 0 ]; then
      failed=$((failed+1))
    fi
    cnt=$((cnt+1))
  done
  echo "tried to fetch $cnt segments ($failed failed)."
done
exit 0
