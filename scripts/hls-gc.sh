#!/bin/bash

INTERVAL=${INTERVAL:-10}

set -e
if [ -z "$1" ] || [ -z "$2" ]; then
   echo "Usage: $0 <base-directory> <seconds-max-age>"
   exit 1
fi
BASE_D="$1"
MAX_AGE="$2"

if [ "$MAX_AGE" -lt 1 ]; then
   echo "ERROR: $MAX_AGE is invalid"
   exit 1
fi

echo "pruning HLS snippets older than $MAX_AGE seconds inside $BASE_D"
cd "$BASE_D"

set +e
while sleep $INTERVAL; do
   files=$(find -name '*.ts')
   threshold=$(date '+%s' -d "$MAX_AGE seconds ago")
   cnt=0
   for file in $files; do
      timestamp=$(basename $file .ts)
      if [ "$timestamp" -lt "$threshold" ]; then
         rm -f "$file"
         cnt=$((cnt+1))
      fi
   done
   echo "pruned $cnt files."
done
exit 0
